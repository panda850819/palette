import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.*;

class palette{
	public static void main(String args[])throws IOException{
    BufferedImage img = null;
    File f = null;

    //read image
    try{
      //route	
      f = new File("palette\\lena.jpg"); 
      img = ImageIO.read(f);
    }catch(IOException e){
      System.out.println(e);
    }

  	//get image width and height
    int width = img.getWidth();
    int height = img.getHeight();


    BufferedImage buff = new BufferedImage(width,height,BufferedImage.TYPE_BYTE_INDEXED);
    //Constructs a ColorModel that translates pixels of the specified number of bits to color/alpha components.
    ColorModel cm = buff.getColorModel();
    //The IndexColorModel class is a ColorModel class that works with pixel values consisting of a single sample that is an index into a fixed colormap in the default sRGB color space. 
    IndexColorModel icm = (IndexColorModel) cm;
    int palette[] = new int[icm.getMapSize()]; //the size is 256
    icm.getRGBs(palette);

    //convert to 
    for(int y = 0; y < height; y++){
      for(int x = 0; x < width; x++){
        int p = img.getRGB(x,y);
        buff.setRGB(x,y,p);
      }
    }


  	//write image
    try{
      f = new File("palette\\LenaOutput.jpg");
      ImageIO.write(buff, "jpg", f);
    }catch(IOException e){
      System.out.println(e);
    }
  }//main() ends here
}